﻿Imports System
Imports ESRI.ArcGIS.Framework
Imports ESRI.ArcGIS.esriSystem
Imports ESRI.ArcGIS.ArcMap
Imports ESRI.ArcGIS.ArcMapUI
Imports ESRI.ArcGIS.Carto
Imports ESRI.ArcGIS.SystemUI
Imports ESRI.ArcGIS.Desktop
Imports ESRI.ArcGIS.Display
Imports ESRI.ArcGIS.Geodatabase
Imports ESRI.ArcGIS.Geometry
Imports ESRI.ArcGIS.Geoprocessing
Imports ESRI.ArcGIS.Location
Imports ESRI.ArcGIS.LocationUI
Imports ESRI.ArcGIS.GeoDatabaseUI
Imports System.Configuration
Public Class CreateTaxCodesByPrecinct
    Inherits ESRI.ArcGIS.Desktop.AddIns.Button
    Public featureCursor As IFeatureCursor
    Public workspace As IWorkspace
    Public prec As String = ""
    Public precinct As String = "Precinct"
    Public wkspc As String = ""
    Public pApp As IMxApplication
    Public pDir As String = "EmergencyManagement"
    Public projectedCoordinateSystem As IProjectedCoordinateSystem
    Public lyrName As String = "Election_Precincts"

    Public Sub New()

    End Sub

    Protected Overrides Sub OnClick()
        '
        PrepLayers()
        '
        My.ArcMap.Application.CurrentTool = Nothing
    End Sub

    Protected Overrides Sub OnUpdate()
        Enabled = My.ArcMap.Application IsNot Nothing
    End Sub

    Public Sub AddFeature()
        Dim pMxDoc As IMxDocument
        pMxDoc = My.Document
        Dim pMap As IMap
        pMap = pMxDoc.FocusMap
        Dim plyr As ILayer
        Dim lyrName As String = ""
        Dim pFeatureLayer As IFeatureLayer = New FeatureLayer
        Dim pActive As IActiveView
        pActive = pMap
        'pApp = IApplication
        pApp = My.ThisApplication
        Dim pStart As Int32 = 1
        Dim pEnd As Int32 = 1
        Dim mes As String = "Processing Request..."
        Dim u As Integer = 1
        Dim j As Integer = 0
        Dim y As Integer = 1
        Dim r As Integer = 0
        Dim k As String
        'Dim featclassname As String = "LocationofEmergency"
        Dim pLst As New List(Of String)

        'Start editing and new edit operation
        Dim Path As String = "C:\EOC Application\"
        Dim factoryType As Type = Type.GetTypeFromProgID("esriDataSourcesGDB.FileGDBWorkspaceFactory")
        Dim workspaceFactory As IWorkspaceFactory = CType(Activator.CreateInstance(factoryType), IWorkspaceFactory)
        'Check to see if the workspace exists, if so then add k as a converted - integer to string - until it is not a workspace allowing for possible double digits 
        'While workspaceFactory.IsWorkspace(Path & wkspc)
        '    If wkspc.Length = 23 Then
        '        k = CStr(y)
        '        wkspc = wkspc.Substring(0, 19) & k & Mid(wkspc, 20, 4)
        '        pDir = wkspc.Substring(0, 19) & k
        '    End If
        '    If wkspc.Length = 24 Then
        '        k = CStr(y)
        '        wkspc = wkspc.Substring(0, 19) & k & Mid(wkspc, 21, 4)
        '        pDir = wkspc.Substring(0, 19) & k
        '    End If
        '    If wkspc.Length = 25 Then
        '        k = CStr(y)
        '        wkspc = wkspc.Substring(0, 19) & k & Mid(wkspc, 22, 4)
        '        pDir = wkspc.Substring(0, 19) & k
        '    End If
        '    y = y + 1
        'End While

        'generateLayers()
        'wkspc = precinct
        pDir = precinct & prec
        Dim myEnumLyr As IEnumLayer = pMap.Layers
        myEnumLyr.Reset()
        plyr = myEnumLyr.Next
        While Not plyr Is Nothing
            pLst.Add(plyr.Name)
            'myEnumLyr.Next()
            plyr = myEnumLyr.Next
        End While

        While r < pLst.Count
            lyrName = pLst(r)
            If lyrName.Length >= 19 Then
                'MsgBox(lyrName)
                If lyrName.Substring(0, 19) = "LocationofEmergency" Then
                    j = j + 1
                End If
            End If
            r = r + 1
        End While

        If j > 0 Then
            lyrName = "LocationofEmergency" & j
        Else
            lyrName = "LocationofEmergency"
        End If

        LoadProjectedCoordinateSystem()

        If (Not System.IO.Directory.Exists(pDir)) Then
            System.IO.Directory.CreateDirectory(Path & pDir)
        End If

        Dim workspaceName As IWorkspaceName = workspaceFactory.Create(Path, wkspc, Nothing, 0)

        ' Cast the workspace name object to the IName interface and open the workspace.
        Dim Name As IName = CType(workspaceName, IName)
        Dim pWorkspace As IWorkspace = CType(Name.Open(), IWorkspace)

        Dim pFeatureWorkspace As IFeatureWorkspace
        pFeatureWorkspace = pWorkspace

        pActive.Refresh()
    End Sub

    Public Sub PrepLayers()
        Dim pMxApp As IMxApplication
        pMxApp = My.ThisApplication
        Dim pMxDoc As IMxDocument
        pMxDoc = My.Document
        Dim pMap As IMap
        pMap = pMxDoc.FocusMap
        Dim pFtrLyr As IFeatureLayer
        'Dim pFtrSel As IFeatureSelection
        Dim pFtrCsr As IFeatureCursor
        Dim pFtr As IFeature
        ' Get a ref to the Election Precincts layer
        Dim e As Integer = 0
        While e < pMap.LayerCount
            If pMap.Layer(e).Name = lyrName Then
                pFtrLyr = pMap.Layer(e)
                e = e + 1
            Else
                e = e + 1
            End If
        End While

        ' Get a ref to the features in this layer
        pFtrCsr = pFtrLyr.Search(Nothing, False)
        pFtr = pFtrCsr.NextFeature
        Dim f As Integer = pFtr.Fields.FindField("PRECINCT")
        Dim prec As String = pFtr.Value(f)
        MsgBox(prec)
        wkspc = precinct & prec & ".gdb"
        MsgBox(wkspc)
        While Not pFtr Is Nothing
            AddFeature()
            SelectParcels(pFtrLyr, pFtr)
            pFtr = pFtrCsr.NextFeature
            f = pFtr.Fields.FindField("PRECINCT")
            prec = pFtr.Value(f)
            wkspc = precinct & prec & ".gdb"
        End While
        MsgBox("Finished Processing Election Precincts")
    End Sub

    Public Function LoadProjectedCoordinateSystem() As IProjectedCoordinateSystem
        Dim spatialReferenceFactory As ISpatialReferenceFactory = New SpatialReferenceEnvironmentClass()
        projectedCoordinateSystem = spatialReferenceFactory.CreateESRISpatialReferenceFromPRJFile("C:\EOC Application\StatePlaneCentral.prj")
        Return ProjectedCoordinateSystem
    End Function

    Public Sub SelectParcels(ByVal pFtrLyr As IFeatureLayer, ByVal pFtr As IFeature)
        Dim pMxApp As IMxApplication
        pMxApp = My.ThisApplication
        Dim pMxDoc As IMxDocument
        pMxDoc = My.Document
        Dim pMap As IMap
        pMap = pMxDoc.FocusMap
        'Dim pFtrLyr As IFeatureLayer
        'Dim pFtrSel As IFeatureSelection
        'Dim pFtrCsr As IFeatureCursor
        'Dim pFtr As IFeature
        Dim pPolygon As IPolygon
        Dim pTopOp As ITopologicalOperator
        Dim buff As Double = 0

        'Set the spatialRelation variable to be called in the Perform Spatial Query sub
        Dim pspatialRelation As esriSpatialRelationEnum
        pspatialRelation = esriSpatialRelationEnum.esriSpatialRelationIntersection

        If pFtrLyr.FeatureClass.ShapeType = 4 and Then
            Dim pPolyBuffer As IPolygon
            ' Get a ref to the polygon and buffer it
            pPolygon = pFtr.Shape
            pTopOp = pPolygon
            pPolyBuffer = pTopOp.Buffer(buff)
            Dim lyrClSID As System.String
            lyrClSID = "{40A9E885-5533-11d0-98BE-00805F7CED21}"
            Dim uid As ESRI.ArcGIS.esriSystem.IUID = New ESRI.ArcGIS.esriSystem.UIDClass
            uid.Value = lyrClSID
            Dim myEnumLyr As IEnumLayer = pMap.Layers(CType(uid, ESRI.ArcGIS.esriSystem.UID))
            'myEnumLyr =
            myEnumLyr.Reset()
            Dim pLayer As ILayer = myEnumLyr.Next
            'myEnumLyr.Reset()
            pLayer = myEnumLyr.Next
            Dim pStr As String
            Dim i As Integer = 0
            While Not pLayer Is Nothing And pLayer.Name <> "Election_Precincts"
                pStr = pLayer.Name
                'MsgBox(pStr)
                If pStr = lyrName Then
                    pLayer = myEnumLyr.Next
                    i = i + 1
                    'MsgBox(i)
                Else
                    Dim pFeat2feature As IFeatureLayer = CType(pLayer, IFeatureLayer)
                    Dim pFeat2 As IFeatureClass = pFeat2feature.FeatureClass
                    PerformSpatialQuery(pFeat2, pPolyBuffer, pspatialRelation, "", i)
                    ExporttoTable(pStr, i, wkspc)
                    pLayer = myEnumLyr.Next
                    i = i + 1
                    'MsgBox(i)
                End If
                'MsgBox(pStr)
            End While
        End If
    End Sub

#Region "Perform Spatial Query"

    '''<summary>Creates a spatial query which performs a spatial search for features in the supplied feature class and has the option to also apply an attribute query via a where clause.</summary>
    '''  
    '''<param name="featureClass">An ESRI.ArcGIS.Geodatabase.IFeatureClass</param>
    '''<param name="searchGeometry">An ESRI.ArcGIS.Geometry.IGeometry (Only high-level geometries can be used)</param>
    '''<param name="spatialRelation">An ESRI.ArcGIS.Geodatabase.esriSpatialRelEnum (e.g., esriSpatialRelIntersects)</param>
    '''<param name="whereClause">A System.String, (e.g., "city_name = 'Redlands'").</param>
    '''   
    '''<returns>An IFeatureCursor holding the results of the query will be returned.</returns>
    '''   
    '''<remarks>Call the SpatialQuery method by passing in a reference to the Feature Class, a Geometry used for the search and the spatial operation to be preformed. An exmaple of a spatial opertaion would be intersects (e.g., esriSpatialRelEnum.esriSpatialRelContains). If you would like to return everything found by the spatial operation use "" for the where clause. Optionally a whereclause (e.g. "income > 1000") maybe applied if desired. The SQL syntax used to specify the where clause is the same as that of the underlying database holding the data.</remarks>
    Public Function PerformSpatialQuery(ByVal featureClass As IFeatureClass, ByVal searchGeometry As IGeometry, ByVal spatialRelation As esriSpatialRelEnum, ByVal whereClause As System.String, ByVal i As Integer) As IFeatureCursor

        ' create a spatial query filter
        Dim spatialFilter As ISpatialFilter = New SpatialFilterClass()

        ' specify the geometry to query with
        spatialFilter.Geometry = searchGeometry

        ' specify what the geometry field is called on the Feature Class that we will be querying against
        Dim nameOfShapeField As System.String = featureClass.ShapeFieldName
        spatialFilter.GeometryField = nameOfShapeField

        ' specify the type of spatial operation to use
        spatialFilter.SpatialRel = spatialRelation

        ' create the where statement
        spatialFilter.WhereClause = whereClause

        ' perform the query and use a cursor to hold the results
        Dim queryFilter As IQueryFilter = New QueryFilterClass()
        queryFilter = CType(spatialFilter, IQueryFilter)
        FeatureCursor = featureClass.Search(queryFilter, False)

        Dim pMxDoc As IMxDocument = My.Document
        Dim pMap As IMap = pMxDoc.FocusMap
        Dim pFeatLyr As IFeatureLayer
        Dim pFeatSel As IFeatureSelection
        pFeatLyr = pMap.Layer(i)
        pFeatSel = pFeatLyr
        pFeatSel.SelectFeatures(queryFilter, esriSelectionResultEnum.esriSelectionResultNew, False)

        Return FeatureCursor

    End Function
#End Region

    Public Sub ExporttoTable(ByVal pStr As String, ByVal i As Integer, ByVal wkspc As String)

        On Error GoTo EH

        Dim pDoc As IMxDocument = My.Document
        Dim pMap As IMap = pDoc.FocusMap

        ' Get the selected layer or table
        Dim pTable As ITable
        Dim pDispTab As IDisplayTable
        Dim pStAloneTab As IStandaloneTable
        Dim pSelItem As ILayer2
        Dim pFeatLay As IFeatureLayer2 = CType(pMap.Layer(i), IFeatureLayer2)
        Dim pFeatClass As IFeatureClass = pFeatLay.FeatureClass
        Dim pDS As IDataset = pFeatClass
        Dim pDataName As IDatasetName = pDS.FullName
        Dim pFCName As IFeatureClassName = New FeatureClassName
        Dim geomDef As IGeometryDef = Nothing
        'Dim pFeatCls As IFeatureClass = pFeatLay.FeatureClass
        'Dim iFeatClsNm As IFeatureClassName = CType(pFeatLay.FeatureClass.AliasName, IFeatureClassName)
        pSelItem = pMap.Layer(i) 'pDoc.SelectedItem
        If Mid(pFeatClass.AliasName, 1, 19) = "LocationofEmergency" Then
            Exit Sub
        End If
        If pSelItem Is Nothing Then
            MsgBox("No feature layer or table selected")
            Exit Sub
            ' For a feature layer or standalonetable
        ElseIf TypeOf pSelItem Is IFeatureLayer Or TypeOf pSelItem Is IStandaloneTable Then
            pDispTab = pSelItem
            pTable = pDispTab.DisplayTable
        Else
            MsgBox("Selected item is not a table or feature layer")
            Exit Sub
        End If

        ' Get the selection on the table/layer
        Dim pSelSet As ISelectionSet = pDispTab.DisplaySelectionSet
        If pSelSet.Count = 0 Then Exit Sub 'pSelSet = Nothing

        ' Get the Definition query (if there is one)
        Dim pTableDef As ITableDefinition
        Dim pQFlt As IQueryFilter
        pQFlt = New QueryFilter
        'pQFlt = Nothing
        pTableDef = pDispTab
        If Not pTableDef.DefinitionExpression = "" Then
            pQFlt.WhereClause = pTableDef.DefinitionExpression
        Else
            pQFlt.WhereClause = "OBJECTID > 0"
        End If

        ' Get the dataset name for the input table
        Dim pDataSet As IDataset
        Dim pDSName As IDatasetName
        pDataSet = pTable
        pDSName = pDataSet.FullName

        Dim Path As String = "C:\EOC Application\"
        Dim factoryType As Type = Type.GetTypeFromProgID("esriDataSourcesGDB.FileGDBWorkspaceFactory")
        Dim factoryType1 As Type = Type.GetTypeFromProgID("esriDataSourcesGDB.AccessWorkspaceFactory")
        Dim workspaceFactory As IWorkspaceFactory = CType(Activator.CreateInstance(factoryType), IWorkspaceFactory)
        Dim workspaceFactory1 As IWorkspaceFactory = CType(Activator.CreateInstance(factoryType1), IWorkspaceFactory)
        Dim pFileName As IFileNames = New FileNames
        pFileName.Add(wkspc)

        If workspaceFactory.IsWorkspace(Path & wkspc) Then
            'If workspaceFactory.ContainsWorkspace(Path, pFileName) Then
            workspace = workspaceFactory.OpenFromFile(Path & wkspc, 0)
            Dim workspaceDataset As IDataset = CType(workspace, IDataset)
            Dim workspaceName As IName = workspaceDataset.FullName
            Dim pName As IName = CType(workspaceName, IName)

            'Set the table name
            Dim pOutDSName As IDatasetName = New TableName
            pOutDSName.Name = pStr
            pOutDSName.WorkspaceName = pName

            'Set the output Feature Class name
            pFCName.FeatureType = pFeatClass.FeatureType
            pFCName.ShapeFieldName = pFeatClass.ShapeFieldName
            pFCName.ShapeType = pFeatClass.ShapeType

            Dim inGeoDef As IGeometryDef = New GeometryDef
            inGeoDef = Nothing

            Dim pTabletoConvert As String = "C:\EOC Application\" & wkspc & "\" & pStr

            ' Export (Selection set) to Geodatabase
            Dim pExpOp As IExportOperation
            pExpOp = New ExportOperation
            pExpOp.ExportTable(pDSName, pQFlt, pSelSet, pOutDSName, 0)

            RunTableToTable(pTabletoConvert, pStr, pDir, pFeatLay, workspace)

            'Create an array to store the names of the feature classes in the new gdb and then call a seperate function later, passing the array, to add them to the map...
            'pArray.Add(pStr + "_1")

        Else
            MsgBox("No workspace found")
        End If

EH:
        If Err.Number = -2147220654 Then
            Exit Sub
        ElseIf Err.Number = 0 Then
        Else
            MsgBox(Err.Number & " " & Err.Description)
        End If

        Exit Sub
    End Sub

    Public Sub RunTableToTable(ByVal pTablePath As String, ByVal pStr As String, ByVal pDir As String, ByVal pFeatLay As IFeatureLayer2, ByVal pWkspc As IWorkspace)

        ' Initialize the geoprocessor.
        Dim GP As GeoProcessor = New GeoProcessor()

        ' Create an IVariantArray to hold the parameter values.
        'Dim parameters As IVariantArray = New VarArray
        'Dim outNm As String = pStr & ".dbf"
        '' Populate the variant array with parameter values.
        'parameters.Add(pTablePath)
        'parameters.Add("C:\EOC Application\" & pDir)
        'parameters.Add(outNm)

        'Create an IVariantArray to hold the parameter values.
        Dim parameterb As IVariantArray = New VarArray
        parameterb.Add(pFeatLay)
        parameterb.Add(pWkspc)
        ' Execute the model tools by name.
        'GP.Execute("TableToTable_conversion", parameters, Nothing)
        GP.Execute("FeatureClassToGeodatabase_conversion", parameterb, Nothing)

    End Sub


End Class
